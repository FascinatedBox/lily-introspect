lily-introspect
===============

# Warning

This repository is obsolete. The interpreter now provides a predefined
introspection module. No further updates will be done to this repo.

[View Documentation](https://fascinatedbox.gitlab.io/lily-introspect/introspect/module.introspect.html)

This library provides simple introspection of the symbols available in the
currently-running interpreter. Since the interpreter does not currently have an
API for introspection, this works by using headers copied from the Lily source.

The internals that this library uses are unlikely to change. In the future, this
library might use a proper API exported by the interpreter instead of relying on
internal layout.

This library is written with native code in mind. Internally, some symbols in
foreign modules are not loaded unless the interpreter is completely sure that
they are necessary (dynaload). This library does not export any means of finding
symbols that are not loaded yet. This is in part because the dynaload part of
the interpreter is more likely to change than symbol internals.
