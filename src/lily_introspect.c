/**
library introspect

This library provides simple introspection of the symbols available in the
currently-running interpreter. Since the interpreter does not currently have an
API for introspection, this works by using headers copied from the Lily source.

The internals that this library uses are unlikely to change. In the future, this
library might use a proper API exported by the interpreter instead of relying on
internal layout.

This library is written with native code in mind. Internally, some symbols in
foreign modules are not loaded unless the interpreter is completely sure that
they are necessary (dynaload). This library does not export any means of finding
symbols that are not loaded yet. This is in part because the dynaload part of
the interpreter is more likely to change than symbol internals.
*/

#include "lily_vm.h"
#include "lily_parser.h"
#include "lily_core_types.h"
#include "lily_symtab.h"

#include "lily.h"
#include "lily_introspect_bindings.h"

/* Fetch an internal field of a struct, pushing it and returning it as the
   result. */
#define FETCH_FIELD(source_type, inner_type, \
                    target_type, target_field, \
                    push_fn) \
lily_introspect_##source_type *introspect_entry = ARG_##source_type(s, 0); \
inner_type *c = introspect_entry->entry; \
target_type target = c->target_field; \
 \
push_fn(s, target); \
lily_return_top(s);

/* FETCH_FIELD, but with a fallback value in case the field is NULL. */
#define FETCH_FIELD_SAFE(source_type, inner_type, \
                         target_type, target_field, \
                         push_fn, fallback) \
lily_introspect_##source_type *introspect_entry = ARG_##source_type(s, 0); \
inner_type *c = introspect_entry->entry; \
target_type target = c->target_field; \
 \
if (target == NULL) \
    target = fallback; \
 \
push_fn(s, target); \
lily_return_top(s);

#define UNPACK_FIRST_ARG(boxed_type, raw_type) \
lily_introspect_##boxed_type *introspect_entry = ARG_##boxed_type(s, 0); \
raw_type entry = introspect_entry->entry

static void destroy_ClassEntry(lily_introspect_ClassEntry *c)
{
}

static void destroy_EnumEntry(lily_introspect_EnumEntry *c)
{
}

static void destroy_FunctionEntry(lily_introspect_FunctionEntry *f)
{
}

static void destroy_MethodEntry(lily_introspect_MethodEntry *m)
{
}

static void destroy_ModuleEntry(lily_introspect_ModuleEntry *m)
{
}

static void destroy_PropertyEntry(lily_introspect_PropertyEntry *p)
{
}

static void destroy_VarEntry(lily_introspect_VarEntry *v)
{
}

static void destroy_VariantEntry(lily_introspect_VariantEntry *v)
{
}

static void destroy_TypeEntry(lily_introspect_TypeEntry *t)
{
}

/* Lily's symbols all start with a next field and are straightforward to extract
   into one of the boxed fields mentioned above. */
#define BUILD_LIST_FROM(check_func, build_func) \
int i, count = 0; \
 \
while (source_iter) { \
    if (check_func(source_iter)) \
        count++; \
 \
    source_iter = source_iter->next; \
} \
 \
lily_container_val *lv = lily_push_list(s, count); \
 \
for (i = 0;source != NULL;source = source->next) { \
    if (check_func(source)) { \
        build_func(s, source); \
        lily_con_set_from_stack(s, lv, i); \
        i++; \
    } \
} \
 \
lily_return_top(s);

/* This is for properties and methods which take the class as well as a source
   value to wrap over. */
#define BUILD_LIST_FROM_2(check_func, build_func) \
int i, count = 0; \
 \
while (source_iter) { \
    if (check_func(source_iter)) \
        count++; \
 \
    source_iter = source_iter->next; \
} \
 \
lily_container_val *lv = lily_push_list(s, count); \
 \
for (i = 0;source != NULL;source = source->next) { \
    if (check_func(source)) { \
        build_func(s, entry, source); \
        lily_con_set_from_stack(s, lv, i); \
        i++; \
    } \
} \
 \
lily_return_top(s);

/* Flat variants are listed at the toplevel of whatever module they're a part of
   instead of in their enum. Their check function needs to take the module and
   the enum to do a parent check. */
#define BUILD_FLAT_VARIANT_LIST(check_func, build_func) \
int i, count = 0; \
 \
while (source_iter) { \
    if (check_func(source_iter, parent)) \
        count++; \
 \
    source_iter = source_iter->next; \
} \
 \
lily_container_val *lv = lily_push_list(s, count); \
 \
for (i = 0;source != NULL;source = source->next) { \
    if (check_func(source, parent)) { \
        build_func(s, entry, source); \
        lily_con_set_from_stack(s, lv, i); \
        i++; \
    } \
} \
 \
lily_return_top(s);

static int allow_all(void *any)
{
    return 1;
}

static int allow_boxed_classes(lily_boxed_sym *sym)
{
    return sym->inner_sym->item_kind == ITEM_TYPE_CLASS;
}

static int allow_boxed_enums(lily_boxed_sym *sym)
{
    return sym->inner_sym->item_kind == ITEM_TYPE_ENUM;
}

static int allow_boxed_variants(lily_boxed_sym *sym)
{
    return sym->inner_sym->item_kind == ITEM_TYPE_VARIANT;
}

static int allow_boxed_vars(lily_boxed_sym *sym)
{
    return sym->inner_sym->item_kind == ITEM_TYPE_VAR;
}

static int allow_classes(lily_class *cls)
{
    return (cls->flags & CLS_IS_ENUM) == 0;
}

static int allow_enums(lily_class *cls)
{
    return cls->flags & CLS_IS_ENUM;
}

static int allow_defines(lily_var *var)
{
    return var->flags & VAR_IS_READONLY;
}

static int allow_flat_variants(lily_named_sym *sym, lily_class *parent)
{
    return sym->item_kind == ITEM_TYPE_VARIANT &&
           ((lily_variant_class *)sym)->parent == parent;
}

static int allow_methods(lily_named_sym *sym)
{
    return sym->item_kind == ITEM_TYPE_VAR;
}

static int allow_properties(lily_named_sym *sym)
{
    return sym->item_kind == ITEM_TYPE_PROPERTY;
}

static int allow_scoped_variants(lily_named_sym *sym)
{
    return sym->item_kind == ITEM_TYPE_VARIANT;
}

static int allow_vars(lily_var *var)
{
    return (var->flags & VAR_IS_READONLY) == 0;
}

static void make_class(lily_state *s, lily_class *source)
{
    lily_introspect_ClassEntry *new_entry = INIT_ClassEntry(s);
    new_entry->entry = source;
}

static void make_enum(lily_state *s, lily_class *source)
{
    lily_introspect_EnumEntry *new_entry = INIT_EnumEntry(s);
    new_entry->entry = source;
}

static void make_function(lily_state *s, lily_var *source)
{
    lily_introspect_FunctionEntry *new_entry = INIT_FunctionEntry(s);
    new_entry->entry = source;
}

static void make_method(lily_state *s, lily_class *entry,
                        lily_named_sym *source)
{
    lily_introspect_MethodEntry *new_entry = INIT_MethodEntry(s);
    new_entry->entry = (lily_var *)source;
    new_entry->parent = entry;
}

static void make_module(lily_state *s, lily_module_entry *source) {
    lily_introspect_ModuleEntry *new_entry = INIT_ModuleEntry(s);
    new_entry->entry = source;
}

static void make_module_from_link(lily_state *s, lily_module_link *source) {
    lily_introspect_ModuleEntry *new_entry = INIT_ModuleEntry(s);
    new_entry->entry = source->module;
}

static void make_property(lily_state *s, lily_class *entry,
                          lily_named_sym *source)
{
    lily_introspect_PropertyEntry *new_entry = INIT_PropertyEntry(s);
    new_entry->entry = (lily_prop_entry *)source;
    new_entry->parent = entry;
}

static void make_var(lily_state *s, lily_var *source)
{
    lily_introspect_VarEntry *new_entry = INIT_VarEntry(s);
    new_entry->entry = source;
}

static void make_variant(lily_state *s, lily_class *entry,
                         lily_named_sym *source)
{
    lily_introspect_VariantEntry *new_entry = INIT_VariantEntry(s);
    new_entry->entry = (lily_variant_class *)source;
    new_entry->parent = entry;
}

static void boxed_make_class(lily_state *s, lily_boxed_sym *source)
{
    make_class(s, (lily_class *)source->inner_sym);
}

static void boxed_make_enum(lily_state *s, lily_boxed_sym *source)
{
    make_enum(s, (lily_class *)source->inner_sym);
}

static void boxed_make_var(lily_state *s, lily_boxed_sym *source)
{
    make_var(s, (lily_var *)source->inner_sym);
}

static void boxed_make_variant(lily_state *s, lily_boxed_sym *source)
{
	lily_variant_class *v = (lily_variant_class *)source->inner_sym;

    make_variant(s, v->parent, (lily_named_sym *)v);
}

/* Unpack the first argument given and send the type it has. All foreign classes
   must have lily_var * as the first field. Internally, the type is at a common
   location for properties, definitions, and vars. This library only needs to
   guarantee the former for this to work across symbols. */
static void unpack_and_return_type(lily_state *s)
{
    UNPACK_FIRST_ARG(FunctionEntry, lily_var *);
    lily_type *t = entry->type;

    lily_introspect_TypeEntry *boxed_type = INIT_TypeEntry(s);
    boxed_type->entry = t;

    lily_return_top(s);
}

/**
foreign class TypeEntry {
    layout {
        lily_type *entry;
    }
}

This is a foreign class that wraps over a Lily type (struct lily_type *) of the
interpreter. It currently doesn't contain a way to extract the members because
that involves forward classes.
*/

/**
define TypeEntry.as_string: String

Return a string that describes the type provided. This uses the same type
printing that the interpreter's error messages use.
*/
void lily_introspect_TypeEntry_as_string(lily_state *s)
{
    UNPACK_FIRST_ARG(TypeEntry, lily_type *);
    lily_msgbuf *msgbuf = lily_mb_flush(lily_msgbuf_get(s));

    lily_mb_add_fmt(msgbuf, "^T", entry);
    lily_push_string(s, lily_mb_raw(msgbuf));
    lily_return_top(s);
}

/**
foreign class VarEntry {
    layout {
        lily_var *entry;
    }
}

This is a foreign class that wraps over a Lily var.
*/

/**
define VarEntry.line_number: Integer

Return the line number this var was declared on.
*/
void lily_introspect_VarEntry_line_number(lily_state *s)
{
    lily_introspect_VarEntry *introspect_entry = ARG_VarEntry(s, 0);
    lily_var *entry = introspect_entry->entry;
    lily_return_integer(s, entry->line_num);
}

/**
define VarEntry.name: String

Return the name of the var provided.
*/
void lily_introspect_VarEntry_name(lily_state *s)
{
    FETCH_FIELD(VarEntry, lily_var, const char *, name, lily_push_string);
}

/**
define VarEntry.type: TypeEntry

Return the type of the var provided.
*/
void lily_introspect_VarEntry_type(lily_state *s)
{
    unpack_and_return_type(s);
}

/**
foreign class PropertyEntry {
    layout {
        lily_prop_entry *entry;
        lily_class *parent;
    }
}

This is a foreign class that wraps over a Lily class property of the
interpreter. It doesn't contain methods to fetch the class since that involves
forward classes.
*/

/**
define PropertyEntry.is_private: Boolean

Return `true` if the property is private, `false` otherwise.
*/
void lily_introspect_PropertyEntry_is_private(lily_state *s)
{
    UNPACK_FIRST_ARG(PropertyEntry, lily_prop_entry *);
    lily_return_boolean(s, !!(entry->flags & SYM_SCOPE_PRIVATE));
}

/**
define PropertyEntry.is_protected: Boolean

Return `true` if the property is protected, `false` otherwise.
*/
void lily_introspect_PropertyEntry_is_protected(lily_state *s)
{
    UNPACK_FIRST_ARG(PropertyEntry, lily_prop_entry *);
    lily_return_boolean(s, !!(entry->flags & SYM_SCOPE_PRIVATE));
}

/**
define PropertyEntry.is_public: Boolean

Return `true` if the property is public, `false` otherwise.
*/
void lily_introspect_PropertyEntry_is_public(lily_state *s)
{
    UNPACK_FIRST_ARG(PropertyEntry, lily_prop_entry *);

    int flags = entry->flags & (SYM_SCOPE_PRIVATE | SYM_SCOPE_PROTECTED);

    lily_return_boolean(s, flags == 0);
}

/**
define PropertyEntry.name: String

Return the name of the property.
*/
void lily_introspect_PropertyEntry_name(lily_state *s)
{
    FETCH_FIELD(PropertyEntry, lily_prop_entry, const char *, name, lily_push_string);
}

/**
define PropertyEntry.type: TypeEntry

Return the type of the property.
*/
void lily_introspect_PropertyEntry_type(lily_state *s)
{
    unpack_and_return_type(s);
}

/**
foreign class FunctionEntry {
    layout {
        lily_var *entry;
    }
}

This is a foreign class that wraps over a toplevel function of a package.
*/

/**
define FunctionEntry.name: String

Return the name of the definition provided.
*/
void lily_introspect_FunctionEntry_name(lily_state *s)
{
    FETCH_FIELD(FunctionEntry, lily_var, const char *, name, lily_push_string);
}

/**
define FunctionEntry.line_number: Integer

Return the line number that this function was declared on.
*/
void lily_introspect_FunctionEntry_line_number(lily_state *s)
{
    lily_introspect_VarEntry_line_number(s);
}

/**
define FunctionEntry.type: TypeEntry

Return the type of the definition provided.
*/
void lily_introspect_FunctionEntry_type(lily_state *s)
{
    unpack_and_return_type(s);
}

/**
foreign class MethodEntry {
    layout {
        lily_var *entry;
        lily_class *parent;
    }
}

This is a foreign class that wraps over a method of a class or enum.
*/

/**
define MethodEntry.function_name: String

Return the unqualified name of the function given.
*/
void lily_introspect_MethodEntry_function_name(lily_state *s)
{
    UNPACK_FIRST_ARG(MethodEntry, lily_var *);

    lily_push_string(s, entry->name);
    lily_return_top(s);
}

/**
define MethodEntry.line_number: Integer

Return the line number that this method was declared on.
*/
void lily_introspect_MethodEntry_line_number(lily_state *s)
{
    lily_introspect_VarEntry_line_number(s);
}

/**
define MethodEntry.is_private: Boolean

Return `true` if the method is private, `false` otherwise.
*/
void lily_introspect_MethodEntry_is_private(lily_state *s)
{
    UNPACK_FIRST_ARG(MethodEntry, lily_var *);
    lily_return_boolean(s, !!(entry->flags & SYM_SCOPE_PRIVATE));
}

/**
define MethodEntry.is_protected: Boolean

Return `true` if the method is protected, `false` otherwise.
*/
void lily_introspect_MethodEntry_is_protected(lily_state *s)
{
    UNPACK_FIRST_ARG(MethodEntry, lily_var *);
    lily_return_boolean(s, !!(entry->flags & SYM_SCOPE_PRIVATE));
}

/**
define MethodEntry.is_public: Boolean

Return `true` if the method is public, `false` otherwise.
*/
void lily_introspect_MethodEntry_is_public(lily_state *s)
{
    UNPACK_FIRST_ARG(MethodEntry, lily_var *);

    int flags = entry->flags & (SYM_SCOPE_PRIVATE | SYM_SCOPE_PROTECTED);

    lily_return_boolean(s, flags == 0);
}

/**
define MethodEntry.type: TypeEntry

Return the type of the method provided.
*/
void lily_introspect_MethodEntry_type(lily_state *s)
{
    unpack_and_return_type(s);
}

/**
foreign class ClassEntry {
    layout {
        lily_class *entry;
    }
}

This is a foreign class that wraps over a Lily class.
*/

/**
define ClassEntry.is_foreign: Boolean

Return 'true' if the class is native (has fields) or 'false' if the class is
foreign (defined in C, no fields). Note that foreign libraries can declare
native classes (classes with fields).
*/
void lily_introspect_ClassEntry_is_foreign(lily_state *s)
{
    lily_introspect_ClassEntry *introspect_entry = ARG_ClassEntry(s, 0);
    lily_class *entry = introspect_entry->entry;
    int is_foreign = !!(entry->flags & CLS_IS_FOREIGN);

    lily_return_boolean(s, is_foreign);
}

/**
define ClassEntry.is_native: Boolean

This is the reverse of 'ClassEntry.is_foreign'.
*/
void lily_introspect_ClassEntry_is_native(lily_state *s)
{
    lily_introspect_ClassEntry *introspect_entry = ARG_ClassEntry(s, 0);
    lily_class *entry = introspect_entry->entry;
    int is_native = !(entry->flags & CLS_IS_FOREIGN);

    lily_return_boolean(s, is_native);
}

/**
define ClassEntry.methods: List[MethodEntry]

Return the methods that were declared in this class. There is no guarantee as to
the order. The constructor's name is <new> to prevent it from being named.
*/
void lily_introspect_ClassEntry_methods(lily_state *s)
{
    lily_introspect_ClassEntry *introspect_entry = ARG_ClassEntry(s, 0);
    lily_class *entry = introspect_entry->entry;
    lily_named_sym *source = entry->members;
    lily_named_sym *source_iter = source;

    BUILD_LIST_FROM_2(allow_methods, make_method);
}

/**
define ClassEntry.module_path: String

Return the path of the module that this class belongs to.
*/
void lily_introspect_ClassEntry_module_path(lily_state *s)
{
    lily_introspect_ClassEntry *introspect_entry = ARG_ClassEntry(s, 0);
    lily_module_entry *m = introspect_entry->entry->module;

    lily_push_string(s, m->path);
    lily_return_top(s);
}

/**
define ClassEntry.name: String

Return the name of the class provided.
*/
void lily_introspect_ClassEntry_name(lily_state *s)
{
    FETCH_FIELD(ClassEntry, lily_class, const char *, name, lily_push_string);
}

/**
define ClassEntry.parent: Option[ClassEntry]

If this class inherits from another, this returns that class in a 'Some'.
Otherwise, this returns 'None'.
*/
void lily_introspect_ClassEntry_parent(lily_state *s)
{
    lily_introspect_ClassEntry *introspect_entry = ARG_ClassEntry(s, 0);
    lily_class *entry = introspect_entry->entry;
    lily_class *parent = entry->parent;

    if (parent) {
        lily_container_val *variant = lily_push_some(s);

        lily_introspect_ClassEntry *new_entry = INIT_ClassEntry(s);
        new_entry->entry = parent;

        lily_con_set_from_stack(s, variant, 0);
        lily_return_top(s);
    }
    else
        lily_return_none(s);
}

/**
define ClassEntry.properties: List[PropertyEntry]

Return the properties that were declared on the class provided. If a class has
been loaded, the properties inside are always loaded. This is in contrast to
methods which may not be dynaloaded.
*/
void lily_introspect_ClassEntry_properties(lily_state *s)
{
    lily_introspect_ClassEntry *introspect_entry = ARG_ClassEntry(s, 0);
    lily_class *entry = introspect_entry->entry;
    lily_named_sym *source = entry->members;
    lily_named_sym *source_iter = source;

    BUILD_LIST_FROM_2(allow_properties, make_property);
}


/**
foreign class VariantEntry {
    layout {
        lily_variant_class *entry;
        lily_class *parent;
    }
}

This is a foreign class that wraps over a Lily enum variant, which is stored
inside of a Lily variant class.
*/

/**
define VariantEntry.is_empty: Boolean

Returns true if the variant is empty, false otherwise. A variant is 'empty' if
it does not receive any arguments.
*/
void lily_introspect_VariantEntry_is_empty(lily_state *s)
{
    lily_introspect_VariantEntry *introspect_entry = ARG_VariantEntry(s, 0);
    lily_variant_class *entry = introspect_entry->entry;

    lily_return_boolean(s, !!(entry->flags & CLS_EMPTY_VARIANT));
}

/**
define VariantEntry.is_scoped: Boolean

Returns true if the variant is scoped, false otherwise. A variant is scoped if
the enum was prefixed with 'scoped' during declarations. Scoped variants must be
qualified with their names to be used, whereas flat variants are directly
available.
*/
void lily_introspect_VariantEntry_is_scoped(lily_state *s)
{
    lily_introspect_VariantEntry *introspect_entry = ARG_VariantEntry(s, 0);
    lily_class *parent = introspect_entry->parent;

    lily_return_boolean(s, !!(parent->flags & CLS_ENUM_IS_SCOPED));
}

/**
define VariantEntry.module_path: String

Return the path of the module that this variant belongs to.
*/
void lily_introspect_VariantEntry_module_path(lily_state *s)
{
    lily_introspect_VariantEntry *introspect_entry = ARG_VariantEntry(s, 0);
    lily_module_entry *m = introspect_entry->entry->parent->module;

    lily_push_string(s, m->path);
    lily_return_top(s);
}

/**
define VariantEntry.name: String

Return the name of the variant provided.
*/
void lily_introspect_VariantEntry_name(lily_state *s)
{
    FETCH_FIELD(VariantEntry, lily_variant_class, const char *, name,
            lily_push_string);
}

/**
define VariantEntry.type: TypeEntry

Return the type of the method provided.
*/
void lily_introspect_VariantEntry_type(lily_state *s)
{
    unpack_and_return_type(s);
}

/**
foreign class EnumEntry {
    layout {
        lily_class *entry;
    }
}

This is a foreign class that wraps over a Lily enum, which is stored inside of a
Lily class.
*/

/**
define EnumEntry.methods: List[MethodEntry]

Return the methods that were declared in this class. There is no guarantee as to
the order. The constructor's name is <new> to prevent it from being named.
*/
void lily_introspect_EnumEntry_methods(lily_state *s)
{
    lily_introspect_EnumEntry *introspect_entry = ARG_EnumEntry(s, 0);
    lily_class *entry = introspect_entry->entry;
    lily_named_sym *source = entry->members;
    lily_named_sym *source_iter = source;

    BUILD_LIST_FROM_2(allow_methods, make_method);
}

/**
define EnumEntry.variants: List[VariantEntry]

Return the variants that were declared within this enum. No ordering is
guaranteed.
*/
void lily_introspect_EnumEntry_variants(lily_state *s)
{
    /* Variants come in two flavors:
       * Flat variants (like Some and None) are found in the module with the
         enum as their parent.
       * Scoped variants (the other kind) are found in the enum.
       The variants have the same item kind either way. */
    lily_introspect_EnumEntry *introspect_entry = ARG_EnumEntry(s, 0);
    lily_class *entry = introspect_entry->entry;
    int is_scoped = entry->flags & CLS_ENUM_IS_SCOPED;

    if (is_scoped) {
        lily_named_sym *source = entry->members;
        lily_named_sym *source_iter = source;

        BUILD_LIST_FROM_2(allow_scoped_variants, make_variant);
    }
    else {
        lily_named_sym *source = entry->members;
        lily_named_sym *source_iter = source;
        lily_class *parent = entry;

        BUILD_FLAT_VARIANT_LIST(allow_flat_variants, make_variant);
    }
}

/**
define EnumEntry.name: String

Return the name of the class provided.
*/
void lily_introspect_EnumEntry_name(lily_state *s)
{
    lily_introspect_ClassEntry_name(s);
}

/**
define EnumEntry.module_path: String

Return the path of the module that this enum belongs to.
*/
void lily_introspect_EnumEntry_module_path(lily_state *s)
{
    lily_introspect_ClassEntry_module_path(s);
}

/**
foreign class ModuleEntry {
    layout {
        lily_module_entry *entry;
    }
}

This is a foreign class that wraps over a Lily module entry. Dont bother the
module entry inside.
*/

/**
define ModuleEntry.boxed_classes: List[ClassEntry]

Return the user-defined classes loaded into this module through direct import
(`import (someclass) somefile`).
*/
void lily_introspect_ModuleEntry_boxed_classes(lily_state *s)
{
    lily_introspect_ModuleEntry *introspect_entry = ARG_ModuleEntry(s, 0);
    lily_module_entry *entry = introspect_entry->entry;
    lily_boxed_sym *source = entry->boxed_chain;
    lily_boxed_sym *source_iter = source;

    BUILD_LIST_FROM(allow_boxed_classes, boxed_make_class)
}

/**
define ModuleEntry.boxed_enums: List[EnumEntry]

Return the user-defined enums loaded into this module through direct import
(`import (someenum) somefile`).
*/
void lily_introspect_ModuleEntry_boxed_enums(lily_state *s)
{
    lily_introspect_ModuleEntry *introspect_entry = ARG_ModuleEntry(s, 0);
    lily_module_entry *entry = introspect_entry->entry;
    lily_boxed_sym *source = entry->boxed_chain;
    lily_boxed_sym *source_iter = source;

    BUILD_LIST_FROM(allow_boxed_enums, boxed_make_enum)
}

/**
define ModuleEntry.boxed_variants: List[VariantEntry]

Return the user-defined variant loaded into this module through direct import
(`import (somevariant) somefile`).
*/
void lily_introspect_ModuleEntry_boxed_variants(lily_state *s)
{
    lily_introspect_ModuleEntry *introspect_entry = ARG_ModuleEntry(s, 0);
    lily_module_entry *entry = introspect_entry->entry;
    lily_boxed_sym *source = entry->boxed_chain;
    lily_boxed_sym *source_iter = source;

    BUILD_LIST_FROM(allow_boxed_variants, boxed_make_variant)
}

/**
define ModuleEntry.boxed_vars: List[VarEntry]

Return the user-defined var loaded into this module through direct import
(`import (somevar) somefile`).
*/
void lily_introspect_ModuleEntry_boxed_vars(lily_state *s)
{
    lily_introspect_ModuleEntry *introspect_entry = ARG_ModuleEntry(s, 0);
    lily_module_entry *entry = introspect_entry->entry;
    lily_boxed_sym *source = entry->boxed_chain;
    lily_boxed_sym *source_iter = source;

    BUILD_LIST_FROM(allow_boxed_vars, boxed_make_var)
}

/**
define ModuleEntry.classes: List[ClassEntry]

Return the user-defined classes within the module. Note that classes that are
not yet dynaloaded will not be included.
*/
void lily_introspect_ModuleEntry_classes(lily_state *s)
{
    /* Do not use boxed elements too. Those are another module's elements. */
    lily_introspect_ModuleEntry *introspect_entry = ARG_ModuleEntry(s, 0);
    lily_module_entry *entry = introspect_entry->entry;
    lily_class *source = entry->class_chain;
    lily_class *source_iter = source;

    BUILD_LIST_FROM(allow_classes, make_class)
}

/**
define ModuleEntry.dirname: String

Return the directory of this module relative to [main].
*/
void lily_introspect_ModuleEntry_dirname(lily_state *s)
{
    FETCH_FIELD_SAFE(ModuleEntry, lily_module_entry, const char *, dirname,
           lily_push_string, "");
}

/**
define ModuleEntry.enums: List[EnumEntry]

Return the user-defined enums within the module. Note that enums that are not
yet dynaloaded will not be included.
*/
void lily_introspect_ModuleEntry_enums(lily_state *s)
{
    /* Do not use boxed elements too. Those are another module's elements. */
    lily_introspect_ModuleEntry *introspect_entry = ARG_ModuleEntry(s, 0);
    lily_module_entry *entry = introspect_entry->entry;
    lily_class *source = entry->class_chain;
    lily_class *source_iter = source;

    BUILD_LIST_FROM(allow_enums, make_enum)
}

/**
define ModuleEntry.functions: List[FunctionEntry]

Return the toplevel definitions inside of this module.
*/
void lily_introspect_ModuleEntry_functions(lily_state *s)
{
    /* Do not use boxed elements too. Those are another module's elements. */
    lily_introspect_ModuleEntry *introspect_entry = ARG_ModuleEntry(s, 0);
    lily_module_entry *entry = introspect_entry->entry;
    lily_var *source = entry->var_chain;
    lily_var *source_iter = source;

    BUILD_LIST_FROM(allow_defines, make_function)
}

/**
define ModuleEntry.modules_used: List[ModuleEntry]

Return the modules that were used inside of this module.
*/
void lily_introspect_ModuleEntry_modules_used(lily_state *s)
{
    lily_introspect_ModuleEntry *introspect_entry = ARG_ModuleEntry(s, 0);
    lily_module_entry *entry = introspect_entry->entry;
    lily_module_link *source = entry->module_chain;
    lily_module_link *source_iter = source;

    BUILD_LIST_FROM(allow_all, make_module_from_link);
}

/**
define ModuleEntry.name: String

Return the name of this module (field: loadname)
*/
void lily_introspect_ModuleEntry_name(lily_state *s)
{
    FETCH_FIELD_SAFE(ModuleEntry, lily_module_entry, const char *, loadname,
            lily_push_string, "[main]");
}

/**
define ModuleEntry.path: String

Return the path used to load the module. Registered modules have a path that is
enclosed in brackets (ex: `[sys]`).
*/
void lily_introspect_ModuleEntry_path(lily_state *s)
{
    FETCH_FIELD(ModuleEntry, lily_module_entry, const char *, path,
            lily_push_string);
}

/**
define ModuleEntry.vars: List[VarEntry]

Return the vars inside the module. Vars that are not yet dynaloaded will not be
included.
*/
void lily_introspect_ModuleEntry_vars(lily_state *s)
{
    /* Do not use boxed elements too. Those are another module's elements. */
    lily_introspect_ModuleEntry *introspect_entry = ARG_ModuleEntry(s, 0);
    lily_module_entry *entry = introspect_entry->entry;
    lily_var *source = entry->var_chain;
    lily_var *source_iter = source;

    BUILD_LIST_FROM(allow_vars, make_var)
}

/**
define module_list: List[ModuleEntry]

Fetch all modules that the interpreter has loaded. No order is guaranteed.
*/
void lily_introspect__module_list(lily_state *s)
{
    lily_parse_state *parser = s->gs->parser;
    lily_module_entry *source = parser->module_start;
    lily_module_entry *source_iter = source;

    BUILD_LIST_FROM(allow_all, make_module);
}

LILY_DECLARE_INTROSPECT_CALL_TABLE
